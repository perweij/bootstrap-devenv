include Makefile.projtools
Makefile.projtools:
	curl    -o Makefile.fetched \
		-L "https://gitlab.com/perweij/gitlab-tools/-/raw/master/Makefile.projtools"
		echo "16e3b2425760277b0bf6a4acbed9fa9f858d2dbedd21ce4eb3950df8e99b1bfa *Makefile.fetched" \
		| sha256sum --check - \
		&& mv Makefile.fetched $@ || rm -f Makefile.fetched


# include definition of SYNKDIRS (avoid ~, use $(HOME) in that file)
include settings.mk
USERROOT    = $(HOME)/root
TOOLREPODIR = $(HOME)/proj
TOOLREPOS   = git@gitlab.com:perweij/bitwarden-dir-synk.git \
	      git@gitlab.com:perweij/bitwarden-file-tool.git \
	      git@gitlab.com:perweij/gitlab-tools.git \
              git@gitlab.com:perweij/bitwarden-vault-for-scripts.git \
              git@gitlab.com:perweij/commando-init.el.git \
	      git@gitlab.com:perweij/mastodon-poster.git \
              git@gitlab.com:perweij/turbovnc-autoreconnect.git \
	      git@gitlab.com:perweij/bootstrap-devenv.git


.PHONY: all dir mod-bashconf os-pkgs poetry bw pull-toolrepos login-bw sync-secrets links sshconfig
all: dir mod-bashconf os-pkgs poetry update-bw login-bw pull-toolrepos sync-secrets links sshconfig


dir:
	mkdir -p $(USERROOT)/stow $(USERROOT)/bin


mod-bashconf:
	src/setup_dirs.pl


os-pkgs:
	sudo apt-get -y update && sudo apt-get -y install --no-install-recommends jq stow git curl wget ed nano wireguard resolvconf cookiecutter policykit-1-gnome gnome-flashback gnome-power-manager


poetry:
	cd /tmp
	export POETRY_HOME=$(HOME)/root
	curl -sSL https://install.python-poetry.org | python3 -


bw:
	rm -f /tmp/bw.zip
	cd /tmp && wget -O bw.zip 'https://vault.bitwarden.com/download/?app=cli&platform=linux' \
	&& unzip bw.zip \
	&& sudo mv -f bw "$(USERROOT)/bin/" || rm -f /tmp/bw /tmp/bw.zip


update-bw:
	command -v bw || $(MAKE) bw


$(TOOLREPODIR)/bitwarden-dir-synk: update-bw login-bw sync-secrets


pull-toolrepos: $(TOOLREPODIR)/bitwarden-dir-synk
	mkdir -p $(TOOLREPODIR) || exit 1
	cd $(TOOLREPODIR) || exit 1
	for REPO in $(TOOLREPOS); do
		name=$$(basename "$$REPO" | sed 's|\.git$$||')
		if [ ! -d "$$name" ]; then
			git clone "$$REPO" $$name || exit 1
		else
			cd "$$name"
			git pull
			cd -
		fi
		if [ -d "$$name"/src ]; then
		  for F in $$(find "$$name"/src -type f -print | xargs readlink -f); do
		    if [ ! -e $(USERROOT)/bin/$$(basename "$$F") ]; then
		      ln -s "$$F" $(USERROOT)/bin/
		    fi
		  done
		fi
	done


status-toolrepos: $(TOOLREPODIR)/bitwarden-dir-synk
	@mkdir -p $(TOOLREPODIR) || exit 1
	cd $(TOOLREPODIR) || exit 1
	for REPO in $(TOOLREPOS); do
		name=$$(basename "$$REPO" | sed 's|\.git$$||')
		echo "#### $$name"
		if [ ! -d "$$name" ]; then
			echo "repo is not checked out" >&2
		else
			cd "$$name" >/dev/null
			git status --porcelain=1
			cd - >/dev/null
		fi
		printf "\n\n\n"
	done


login-bw:
	test $$(bw status | jq -r '.status') = "unauthenticated" && bw login
	test $$(bw status | jq -r '.status') = "locked" && bw unlock
	test $$(bw status | jq -r '.status') = "unlocked" || exit 1


sync-secrets:
	must_clone=false
	for SPAIR in $(SYNKDIRS); do
	   IFS=':' read -r -a SPAIR_ARR <<< "$$SPAIR"
	   DIR="$${SPAIR_ARR[1]}"
	   if [ ! -d "$$DIR" ]; then # FIXME: not a perfekt check, as this is
                                    # only the parent folder
		must_clone=true; break
	   fi
	done
	if "$$must_clone"; then
	  TMPDIR=$$(mktemp -d)
	  cd $$TMPDIR
	  git clone https://gitlab.com/perweij/bitwarden-file-tool.git
	  git clone https://gitlab.com/perweij/bitwarden-dir-synk.git
	  export PATH=$$PATH:$$PWD/bitwarden-dir-synk/src:$$PATH:$$PWD/bitwarden-file-tool/src
	  for SPAIR in $(SYNKDIRS); do
	     IFS=':' read -r -a SPAIR_ARR <<< "$$SPAIR"
	     SECRET="$${SPAIR_ARR[0]}"
	     DIR="$${SPAIR_ARR[1]}"
	     mkdir -p "$$DIR"
	     bitwarden-dir-synk --name "$$SECRET" "$$DIR" || exit 1
	     chmod -R go= "$$DIR"
	  done
	  cd && rm -rf "$$TMPDIR"
	fi
	chmod -R go= ~/.ssh

links:
	if test -d $(HOME)/.git-config && ! test -L $(HOME)/.gitconfig; then ln -s $(HOME)/.git-config/gitconfig $(HOME)/.gitconfig; fi
	if test -d $(HOME)/.git-config && ! test -L $(HOME)/.git-credentials; then ln -s $(HOME)/.git-config/git-credentials $(HOME)/.git-credentials; fi
	if test -d $(HOME)/.emacs.d && test -d $(HOME)/proj/commando-init.el && ! test -L $(HOME)/.emacs.d/init.el; then ln -s $(HOME)/proj/commando-init.el/init.el $(HOME)/.emacs.d/init.el; fi
	if test -d $(HOME)/.authdir && ! test -L $(HOME)/.authinfo; then ln -s $(HOME)/.authdir/authinfo $(HOME)/.authinfo; fi

sshconfig:
	cp $(PWD)/sshconfig $(HOME)/.ssh/config
