#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin qw($Bin);

my $bindir = "~/root/bin";
my $godir = "~/go/bin";
my $i3confdir = "~/.config/i3";

#### create directories
map { system("mkdir -p $_") == 0 || die($!) } map { glob($_) } ($i3confdir);

#### manage PATH
chomp(my @rc = `cat ~/.bashrc`);
foreach my $bin ($bindir, $godir, $Bin) {
    print "search $bin\n";
    if(! grep { $_ eq 'PATH="${PATH:+${PATH}:}'.$bin.'"' } @rc) {
	print STDERR "**** adding to PATH: $bin\n";
    	open(my $f, ">>".glob("~/.bashrc")) || die($!);
	print $f 'PATH="${PATH:+${PATH}:}'.$bin.'"'."\n";
	close($f);
    }
}

#### manage i3
chomp(my @i3conf = `cat $i3confdir/config`);
if(! grep { $_ =~ /toggle_dashboard/ } @i3conf) {
    print STDERR "**** adding conf to i3\n";

    my $conf = <<"END_CONF";
# sudo apt-get install policykit-1-gnome gnome-flashback gnome-power-manager
# run after restarting i3: gsettings set org.gnome.gnome-flashback desktop false
exec --no-startup-id /usr/libexec/gsd-xsettings
exec_always --no-startup-id gnome-power-manager
exec --no-startup-id /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id gnome-flashback
bindsym Menu          exec "i3-msg exec -- nohup $Bin/toggle_dashboard.pl &"
bindsym section       exec "i3-msg exec -- nohup $Bin/toggle_dashboard.pl &"
bindsym \$mod+Menu    exec "i3-msg exec -- nohup $Bin/toggle_dashboard.pl --cheatsheet &"
bindsym \$mod+section exec "i3-msg exec -- nohup $Bin/toggle_dashboard.pl --cheatsheet &"
END_CONF

    open(my $f, ">>".glob("$i3confdir/config")) || die($!);
    print $f $conf;
    close($f);

    print STDERR "***** remember to reload i3 conf\n";
}


#### manage aliases
if( ! -f glob("~/.bash_aliases")) {
    system("touch ~/.bash_aliases");
}
chomp(my @aliases = `cat ~/.bash_aliases`);
if(! grep { $_ =~ /emacs.dump/ } @i3conf) {
    print STDERR "**** adding bash aliases\n";
    open(my $f, ">>".glob("~/.bash_aliases")) || die($!);
    print $f 'alias e="emacs -q --dump-file ~/.emacs.d/emacs.dump -l ~/.emacs.d/load-path.el -e pw/init-agenda"'."\n";
    close($f);
}
