#!/usr/bin/perl -w
use strict;
use warnings;

my $orgdir = "~/proj-bw/org";
if(-d "~/blandat-jobb"
   || -d "~/proj-af") {
    $orgdir = "~/blandat-jobb/org";
}


my $mode = shift // "";
my $arg = "-e pw/init-agenda";

if($mode eq "--cheatsheet") {
    $arg = " $orgdir/cheatsheets.org ";
}

chomp(my @pids = `pgrep --full '^emacs .*--dump-file'`);
if(@pids) {
    map { system("kill $_") } @pids;
} else {
    chdir($orgdir);
    system("emacs -q --dump-file ~/.emacs.d/emacs.dump -l ~/.emacs.d/load-path.el $arg");
}
