sudo apt install -y autoconf automake bsd-mailx dbus-x11 debhelper dpkg-dev \
    gcc-10 libacl1-dev libasound2-dev libdbus-1-dev libgccjit-10-dev \
    libgif-dev libgnutls28-dev libgpm-dev libgtk-3-dev libjansson-dev \
    libjpeg-dev liblcms2-dev liblockfile-dev libm17n-dev libncurses5-dev \
    liboss4-salsa2 libotf-dev libpng-dev librsvg2-dev libselinux1-dev \
    libsystemd-dev libtiff-dev libxi-dev libxml2-dev libxpm-dev libxt-dev \
    procps quilt sharutils texinfo zlib1g-dev gvfs language-pack-en-base \
    libasound2 libaspell15 libasyncns0 libatk-bridge2.0-0 libatk1.0-0 \
    libatspi2.0-0 libbrotli1 libc6 libc6-dev libcairo-gobject2 libcairo2 \
    libcanberra-gtk3-0 libcanberra-gtk3-module libcanberra0 libdatrie1 \
    libdb5.3 libdrm2 libegl1 libepoxy0 libflac8 libfontconfig1 libfreetype6 \
    libgbm1 libgccjit0 libgcc-s1 libgdk-pixbuf2.0-0 libgif7 libgl1 \
    libglvnd0 libglx0 libgpm2 libgraphite2-3 libgstreamer-gl1.0-0 \
    libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libgtk-3-0 \
    libgudev-1.0-0 libharfbuzz-icu0 libharfbuzz0b libhyphen0 libice6 \
    libicu70 libisl23 libjansson4 libjbig0 libjpeg-turbo8 liblcms2-2 \
    liblockfile1 libltdl7 libm17n-0 libmpc3 libmpfr6 libnotify4 libnss-mdns \
    libnss-myhostname libnss-systemd libogg0 liborc-0.4-0 libotf1 \
    libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0 libpixman-1-0 \
    libpng16-16 libpulse0 librsvg2-2 libsasl2-2 libsecret-1-0 libsm6 \
    libsndfile1 libsoup2.4-1 libssl3 libstdc++6 libtdb1 libthai0 libtiff5 \
    libvorbis0a libvorbisenc2 libvorbisfile3 libwayland-client0 \
    libwayland-cursor0 libwayland-egl1 libwayland-server0 libwebp7 \
    libwebpdemux2 libwoff1 libx11-6 libx11-xcb1 libxau6 libxcb-render0 \
    libxcb-shm0 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxdmcp6 \
    libxext6 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxml2 libxpm4 \
    libxrandr2 libxrender1 libxslt1.1 libyajl2 libwebp-dev libtree-sitter0 \
    libtree-sitter-dev emacs-bin-common emacs-common libcairo2-dev \
    libdbus-1-3 libfontconfig1-dev libfreetype6-dev libglib2.0-0 libgmp10 \
    libgnutls30 libgpm2 libharfbuzz0b libjpeg8-dev libtinfo-dev \
    build-essential libxfixes-dev libxrender-dev libsqlite3-dev libc6 \
    libc6-dev libenchant-2-dev

# Clone Emacs master
cd /tmp ; git clone --depth 1 git://git.sv.gnu.org/emacs.git

# Install it

export CC="gcc-10" CXX="gcc-10"
./autogen.sh
# Turns native compilation on. Remove if you don't want this.
./configure --with-json --without-native-compilation --prefix=/usr/local/stow/emacs --with-tree-sitter --with-x
make -j $(nproc)
make install
